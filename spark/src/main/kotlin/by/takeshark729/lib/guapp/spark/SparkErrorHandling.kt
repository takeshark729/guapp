package by.takeshark729.lib.guapp.spark

import by.takeshark729.lib.guapp.app.AppContext
import by.takeshark729.lib.guapp.app.AppException
import by.takeshark729.lib.guapp.app.Runner
import by.takeshark729.lib.guapp.spark.SPARK.TYPE_JSON
import com.google.inject.Inject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spark.Request
import spark.Response
import spark.Spark

/**
 * @author Ilia Kuznetsov
 * @since 6.12.17.
 */
open class SparkErrorHandling @Inject constructor(app: AppContext) : SparkErrorHelper(app), Runner {
    override fun run() {
        routeNotFound()
        routeAppError()
        routeInternalError()
    }

    private fun routeNotFound() = Spark.notFound { _, res ->
        res.type(TYPE_JSON)
        res.status(404)
        responseMsg("URL is not supported.")
    }

    private fun routeInternalError() = Spark.internalServerError { _, res ->
        res.type(TYPE_JSON)
        res.status(500)
        responseMsg("Internal server error")
    }

    private fun routeAppError() = handle(AppException::class.java, 400, warn)
}

@Suppress("MemberVisibilityCanPrivate")
abstract class SparkErrorHelper(app: AppContext) {
    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger("ErrorHandler")
    }

    protected val msgHelp = app.config.string("spark.help") ?: ResponseMessage.DEFAULT_MSG

    protected fun <T: Exception> handle(exType: Class<T>,
                                        status: Int,
                                        body: (T, Request, Response) -> String? = silent) =
            Spark.exception(exType) { ex, rq, rs ->
                rs.reset()
                val msg = body(ex, rq, rs)
                rs.prepare(status, msg)
            }


    protected val warn:(Exception, Request, Response) -> String? =
            {ex, _, _ -> LOGGER.warn("[APP WARN] ${ex.message}"); ex.message}
    protected val err:(Exception, Request, Response) -> String? =
            {ex, _, _ -> LOGGER.error("[APP ERROR] ${ex.message}"); ex.message}
    protected val silent:(Exception, Request, Response) -> String? =
            {ex, _, _ -> ex.message}

    protected fun Response.reset() = raw().reset()

    protected fun Response.prepare(status: Int, msg: String?) {
        msg ?: throw IllegalStateException("Can't parse error")
        type(TYPE_JSON)
        status(status)
        body( responseMsg(msg.escape()) )
    }

    protected fun String.firstLine() = this.split("\n")[0]
    protected fun String.escape() = this.replace("\"", "'")
    protected fun responseMsg(msg: String): String = ResponseMessage(msg, msgHelp).asJson()
}

class ResponseMessage(
        val message: String,
        val help: String = DEFAULT_MSG
) {
    companion object {
        val DEFAULT_MSG = "Please, contact the administrator. (This message is defined as  'spark.help')"
    }

    fun asJson(): String = SPARK.appGson.toJson(this)
}