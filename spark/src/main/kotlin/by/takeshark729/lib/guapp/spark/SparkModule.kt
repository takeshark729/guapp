package by.takeshark729.lib.guapp.spark

import by.takeshark729.lib.guapp.app.AppContext
import by.takeshark729.lib.guapp.app.RunModule
import by.takeshark729.lib.guapp.app.Runner
import com.google.inject.Inject
import spark.Spark

/**
 * @author Ilia Kuznetsov
 * @since 6.12.17.
 */
object SparkModule : RunModule(
        SparkInitRunner::class.java,
        SparkErrorHandling::class.java,
        SparkHealth::class.java)

class SparkInitRunner @Inject constructor(private val app: AppContext): Runner {
    override fun run() {
        val port = app.config.int("spark.port") ?: 8080
        Spark.port(port)
    }
}

class SparkHealth : Runner {
    override fun run() { health() }
    private fun health() = SparkJson.get("/health") { rq, rs -> MSG_OK }
}

class Message(var msg: String)
val MSG_OK = Message("OK")