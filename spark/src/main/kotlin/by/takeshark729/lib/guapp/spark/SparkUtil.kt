package by.takeshark729.lib.guapp.spark

import by.takeshark729.lib.guapp.app.AppException
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import spark.Request
import spark.Response
import spark.Spark

/**
 * @author Ilia Kuznetsov
 * @since 6.12.17.
 */
class InvalidJsonException(msg: String) : AppException(msg)

@Suppress("MemberVisibilityCanPrivate")
object SPARK {
    const val TYPE_JSON = "application/json"
    const val DATE_FORMAT = "dd-MM-YYYY kk:mm:ss"

    val appGson = GsonBuilder()
            .disableHtmlEscaping()
            .serializeNulls()
            .setDateFormat(DATE_FORMAT)
            .create()

    @Suppress("UNCHECKED_CAST")
    fun jsonAsMap(json: String?): Map<String, Any> = safeJson(json) {
        if (json.isNullOrEmpty()) emptyMap()
        else appGson.fromJson(json, Map::class.java) as Map<String, Any>
    }

    fun <T> safeJson(json: String?, body:() -> T) = try {
        body()
    } catch (ex: JsonSyntaxException) {
        throw InvalidJsonException("Can't parse json: $json")
    }
}

object SparkJson {
    fun get(path: String, status: Int = 200, body: (req: Request, res: Response) -> Any) =
            Spark.get(path, { req, res ->
                res.type(SPARK.TYPE_JSON)
                res.status(status)
                body(req, res)
            }, SPARK.appGson::toJson)

    fun post(path: String, status: Int = 200, body: (req: Request, res: Response) -> Any) =
            Spark.post(path, { req, res ->
                res.type(SPARK.TYPE_JSON)
                res.status(status)
                body(req, res)
            }, SPARK.appGson::toJson)

    fun put(path: String, status: Int = 200, body: (req: Request, res: Response) -> Any) =
            Spark.put(path, { req, res ->
                res.type(SPARK.TYPE_JSON)
                res.status(status)
                body(req, res)
            }, SPARK.appGson::toJson)

    fun del(path: String, status: Int = 200, body: (req: Request, res: Response) -> Any) =
            Spark.delete(path, { req, res ->
                res.type(SPARK.TYPE_JSON)
                res.status(status)
                body(req, res)
            }, SPARK.appGson::toJson)
}

fun Response.flushAsList(iter: Iterator<*>) {
    val writer = raw().writer
    writer.write("[")
    if (iter.hasNext()) writer.write( SPARK.appGson.toJson( iter.next() ) )
    while(iter.hasNext()) {
        val next = iter.next()
        writer.write(",")
        writer.write( SPARK.appGson.toJson(next) )
        writer.flush()
    }
    writer.write("]")
    writer.flush()
}
