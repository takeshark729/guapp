package by.takeshark729.lib.guapp.spark

import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.util.ArrayList

class FixedObjectTypeAdapter : TypeAdapter<Any>() {
    val delegate = Gson().getAdapter(Object::class.java)

    override fun read(`in`: JsonReader?): Any? {
        val token = `in`!!.peek()
        when (token) {
            JsonToken.BEGIN_ARRAY -> {
                val list = ArrayList<Any?>()
                `in`.beginArray()
                while (`in`.hasNext()) {
                    list.add(read(`in`))
                }
                `in`.endArray()
                return list
            }

            JsonToken.BEGIN_OBJECT -> {
                val map = LinkedTreeMap<String, Any>()
                `in`.beginObject()
                while (`in`.hasNext()) {
                    map.put(`in`.nextName(), read(`in`))
                }
                `in`.endObject()
                return map
            }

            JsonToken.STRING -> return `in`.nextString()

            JsonToken.NUMBER -> {
                val n = `in`.nextString()
                if (n.indexOf('.') != -1) {
                    return n.toDouble()
                }
                return n.toIntOrNull() ?: n.toLongOrNull()
            }

            JsonToken.BOOLEAN -> return `in`.nextBoolean()

            JsonToken.NULL -> {
                `in`.nextNull()
                return null
            }

            else -> throw IllegalStateException()
        }
    }

    @Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
    override fun write(out: JsonWriter?, value: Any?) = delegate.write(out, value as Object)
}