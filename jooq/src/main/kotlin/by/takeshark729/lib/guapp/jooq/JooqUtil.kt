package by.takeshark729.lib.guapp.jooq

import org.jooq.*
import org.jooq.impl.DSL
import org.jooq.impl.DefaultDataType
import org.jooq.impl.SQLDataType
import java.sql.Date
import java.sql.Time
import java.sql.Timestamp
import kotlin.coroutines.experimental.buildSequence

/**
 * @author Ilia Kuznetsov
 * @since 6.12.17.
 */
fun <T> String.field(clz: Class<T>, tableAlias: String? = null): Field<T> = tableAlias
                ?.let {  DSL.field("${DSL.name(tableAlias)}.${DSL.name(this)}", clz)  }
                ?: DSL.field( DSL.name( this ), clz )

fun String.field(tableAlias: String? = null) = field(Any::class.java, tableAlias)
fun String.intField(tableAlias: String? = null) = field(Int::class.java, tableAlias)
fun String.stringField(tableAlias: String? = null) = field(String::class.java, tableAlias)
fun String.boolField(tableAlias: String? = null) = field(Boolean::class.java, tableAlias)
fun String.dateField(tableAlias: String? = null) = field(Date::class.java, tableAlias)
fun String.doubleField(tableAlias: String? = null) = field(Double::class.java, tableAlias)
fun String.timeField(tableAlias: String? = null) = field(Time::class.java, tableAlias)
fun String.timestampField(tableAlias: String? = null) = field(Timestamp::class.java, tableAlias)

fun <T> Field<T>.of(table: String): Field<T> = name.field(type, table)
fun <T> Field<T>.of(table: Table<*>): Field<T> = of(table.name)

fun String.table() = DSL.table( DSL.name( this ) )
fun String.table(alias: String) = this.table().`as`(alias)
fun String.onlyTable() = DSL.table("only ${DSL.name(this)}")
fun String.onlyTable(alias: String) = DSL.table("only " + DSL.name(this) + " as ${DSL.name(alias)}")
fun String.inline() = DSL.inline(this)

object JOOQ {
    val ID = "Id".intField()
    val CODE = "Code".stringField()
    val DESCRIPTION = "Description".stringField()
    val STATUS = "Status".stringField()
    val USER = "User".stringField()
    val BEGIN_DATE = "BeginDate".dateField()

    val IS_ACTIVE = STATUS.eq("A")

    fun inetValue(value: String?) = DSL.value(value).cast(TYPE_INET)
    val TYPE_INET = DefaultDataType(SQLDialect.POSTGRES, SQLDataType.VARCHAR, "inet")
}

val PATTERN_IP4 = "^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\\.|(\\/(3[0-2]?|[0-2]?[0-9]))?\$)){4}\$"

fun DSLContext.paginate(pageSize: Int = 500,
                        orderBy: Field<*>? = JOOQ.ID,
                        start: Int = 0,
                        limit: Int? = null,
                        body: () -> SelectOrderByStep<*>): kotlin.sequences.Sequence< Map<String, Any> > =
        buildSequence {
            val query = orderBy?.let { body().orderBy(orderBy) } ?: body().orderBy(1)
            val count = count { query }
            val total = limit?.let { Math.min(it, count) } ?: count //
            val size = Math.min(pageSize, total)

            var offset = start
            while (offset < total + start) {
                query.limit(size)
                        .offset(offset)
                        .fetchMaps()
                        .forEach { yield(it) }
                offset += pageSize
            }
        }

fun DSLContext.count(body: () -> SelectLimitStep<*>): Int = fetchCount(body())