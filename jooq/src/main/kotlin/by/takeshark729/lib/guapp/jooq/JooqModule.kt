package by.takeshark729.lib.guapp.jooq

import by.takeshark729.lib.guapp.app.AppContext
import by.takeshark729.lib.guapp.app.RunModule
import com.google.inject.Provides
import com.google.inject.Singleton
import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import javax.sql.DataSource

/**
 * @author Ilia Kuznetsov
 * @since 17.7.17.
 */
object JooqModule : RunModule() {
    override fun configure() { }

    @Provides
    @Singleton
    fun datasource(context: AppContext): DataSource = HikariDataSource().apply {
        with(context.config) {
            jdbcUrl = string("db.url")!!
            username = string("db.username")!!
            password = string("db.password")!!
            maximumPoolSize = int( "db.maxPoolSize") ?: 10
        }
    }

    @Provides
    @Singleton
    fun dialect(): SQLDialect = SQLDialect.POSTGRES_9_4

    @Provides
    @Singleton
    fun dslContext(dataSource: DataSource, dialect: SQLDialect): DSLContext = DSL.using(dataSource, dialect)
}