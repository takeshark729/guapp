package by.takeshark729.lib.guapp.app

import com.google.inject.AbstractModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.yaml.snakeyaml.Yaml
import java.io.FileInputStream
import java.io.InputStream

/**
 * @author Shark729
 * @since 7.8.17.
 */
const val CONFIG_ENV = "APP_CONFIG"
const val ENV_PREFIX = "APP_"
const val DEFAULT_PATH = "config.yml"

class AppModule(private val args: List<String>) : AbstractModule() {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(this::class.java)
    }

    private val path: String by lazy {
        when {
            System.getProperty(CONFIG_ENV) != null -> System.getProperty(CONFIG_ENV)
            System.getenv(CONFIG_ENV) != null -> System.getenv(CONFIG_ENV)
            else -> DEFAULT_PATH
        }
    }

    override fun configure() {
        bind(AppContext::class.java)
                .toInstance(AppContext(args, buildConfig()))
    }

    private fun buildConfig(): ConfigMap {
        val configMap = ConfigMap()
        parseFile(configMap.raw)
        parseEnv(configMap.raw)
        logger.debug("[CONFIG] Application configurations:")
        configMap.raw.forEach { key, value -> logger.info( "   * $key: ${safeValue(key, value)}" ) }
        logger.info(".")
        return configMap
    }

    private fun parseFile(map: MutableMap<String, Any>) {
        val rawMap = readFile()
        flatMap("", rawMap, map)
    }

    private fun readFile(): Map<String, Any> {
        logger.info("[CONFIG] Read configurations from file (environment variable: '${CONFIG_ENV}'): $path")
        val configFile: InputStream = try {
            FileInputStream(path)
        } catch (ex: Exception) {
            try {
                this.javaClass.classLoader.getResourceAsStream(path)
            } catch (ex: Exception) {
                logger.warn("[CONFIG] Failed to load file '$path'. Load default config-file '${DEFAULT_PATH}'")
                try {
                    this.javaClass.classLoader.getResourceAsStream(DEFAULT_PATH)
                } catch (ex: Exception) {
                    logger.warn("[CONFIG] Failed to load file '$DEFAULT_PATH'. No config provided!")
                    "".byteInputStream()
                }
            }
        }
        val result = Yaml().load(configFile)

        @Suppress("UNCHECKED_CAST")
        return result?.let {
                    it as Map<String, Any>
                } ?:emptyMap()
    }

    @Suppress("UNCHECKED_CAST")
    private fun flatMap(prefix: String, data: Map<String, Any?>, result: MutableMap<String, Any>) {
        data.forEach {
            when {
                it.value is Map<*, *> -> flatMap("$prefix${it.key}.", it.value as Map<String, Any>, result)
                it.value == null -> Unit
                else -> result["$prefix${it.key}"] = it.value!!
            }
        }
    }

    private fun parseEnv(result: MutableMap<String, Any>) {
        logger.info("[CONFIG] Read configurations from environment variables with prefix '${ENV_PREFIX}'.")
        System.getenv()
                .filter { it.key != null && it.key.startsWith(ENV_PREFIX) }
                .filter { it.value != null }
                .forEach { result[it.key.substring( ENV_PREFIX.length ) ] = it.value }
    }

    private fun safeValue(key: String, value: Any?) = value?.let {
        if (key.contains("pass")) return "***"
        it
    }
}

data class AppContext(
        val args: List<String>,
        val config: ConfigMap
)

class ConfigMap {
    val raw = mutableMapOf<String, Any>()

    fun string(key: String) = raw[key]?.toString()
    fun int(key: String): Int? = raw[key]?.toString()?.toIntOrNull()
    fun bool(key: String): Boolean? = raw[key]?.toString()?.toBoolean()

    override fun toString(): String = raw.toString()
}

const val ASCII_WELCOME = """
  ▄████  █    ██  ▄▄▄       ██▓███   ██▓███         ▄▄▄▄   ▓█████▄▄▄█████▓ ▄▄▄
 ██▒ ▀█▒ ██  ▓██▒▒████▄    ▓██░  ██▒▓██░  ██▒      ▓█████▄ ▓█   ▀▓  ██▒ ▓▒▒████▄
▒██░▄▄▄░▓██  ▒██░▒██  ▀█▄  ▓██░ ██▓▒▓██░ ██▓▒      ▒██▒ ▄██▒███  ▒ ▓██░ ▒░▒██  ▀█▄
░▓█  ██▓▓▓█  ░██░░██▄▄▄▄██ ▒██▄█▓▒ ▒▒██▄█▓▒ ▒      ▒██░█▀  ▒▓█  ▄░ ▓██▓ ░ ░██▄▄▄▄██
░▒▓███▀▒▒▒█████▓  ▓█   ▓██▒▒██▒ ░  ░▒██▒ ░  ░      ░▓█  ▀█▓░▒████▒ ▒██▒ ░  ▓█   ▓██▒
 ░▒   ▒ ░▒▓▒ ▒ ▒  ▒▒   ▓▒█░▒▓▒░ ░  ░▒▓▒░ ░  ░      ░▒▓███▀▒░░ ▒░ ░ ▒ ░░    ▒▒   ▓▒█░
  ░   ░ ░░▒░ ░ ░   ▒   ▒▒ ░░▒ ░     ░▒ ░           ▒░▒   ░  ░ ░  ░   ░      ▒   ▒▒ ░
░ ░   ░  ░░░ ░ ░   ░   ▒   ░░       ░░              ░    ░    ░    ░        ░   ▒
      ░    ░           ░  ░                         ░         ░  ░              ░  ░
                                                         ░
(text generator: http://patorjk.com)
"""
