package by.takeshark729.lib.guapp.app

/**
 * @author Shark729
 * @since 7.8.17.
 */
open class AppException(msg: String) : RuntimeException(msg)
