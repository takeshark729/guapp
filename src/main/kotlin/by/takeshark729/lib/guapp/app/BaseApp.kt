package by.takeshark729.lib.guapp.app

import com.google.inject.AbstractModule
import com.google.inject.Guice
import com.google.inject.Module
import org.slf4j.LoggerFactory

/**
 * @author Shark729
 * @since 17.7.17.
 */
interface Runner { fun run() }

abstract class RunModule(val runners: Collection<Class<out Runner>>) : AbstractModule() {
    constructor(vararg runners: Class<out Runner>) : this(runners.asList())
    override fun configure() { }
}

abstract class MetaModule : AbstractModule() {
    abstract fun subModules() : List<Module>
}

class App( args: Array<String> = emptyArray() ) {
    companion object {
        val logger = LoggerFactory.getLogger(App::class.java)
    }
    private val modules = mutableListOf<Module>( AppModule(args.asList()) )
    private val runners = mutableListOf<Class<out Runner>>()

    fun module(vararg module: Module): App {
        module.forEach {
            if (it is MetaModule) module( *it.subModules().toTypedArray() )
            modules.add(it)
        }
        return this
    }

    fun runner(vararg runner: Class<out Runner>): App {
        runners.addAll(runner)
        return this
    }

    fun start() {
        println(ASCII_WELCOME)

        logger.info( "[APP]\n\n    *** Get modules & runners (READY): ***\n" )

        modules.filter { it is RunModule }
                .map { it as RunModule }
                .forEach { runners.addAll( it.runners )  }

        logger.info("[APP] Modules:")
        modules.forEach { logger.info( "  * ${it.javaClass.name }") }
        logger.info("[APP] Runners")
        runners.forEach { logger.info( "  * ${it.name }") }


        logger.info( "[APP]\n\n    *** Create Guice injector with modules (STEADY): ***\n" )

        val injector = Guice.createInjector(modules)

        logger.info( "[App]\n\n    *** Run all runners (GO!): ***\n" )

        runners.forEach { injector.getInstance(it).run() }
    }

}