package by.takeshark729.lib.guapp.modules.example

import by.takeshark729.lib.guapp.app.RunModule
import by.takeshark729.lib.guapp.app.Runner
import com.google.inject.Inject
import org.slf4j.LoggerFactory

/**
 * @author Shark729
 * @since 8.8.17.
 */
object ExampleModule : RunModule(
        ExampleRunner::class.java)

class ExampleService {
    companion object {
        val logger = LoggerFactory.getLogger("Example")
    }

    fun sayHello() {
        say( helloMessage() )
    }

    fun say(msg: String) {
        logger.info( msg )
    }

    fun helloMessage() = "Hello from example service!"
}

class ExampleRunner @Inject constructor( val service: ExampleService) : Runner {
    override fun run() {
        // Do initalizations here, after app was started

        service.sayHello()
    }
}
